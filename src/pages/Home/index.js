import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';

export default function Home({ navigation }){
    function navigateToSobre() {
        navigation.navigate('Sobre')
    }
    return (
        <View>
            <Text> index </Text>
            <Button title="navigate" onPress={navigateToSobre}/>
        </View>
    );
}
