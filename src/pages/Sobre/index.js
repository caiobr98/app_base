import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';

export default function Home({ navigation }) {
    function navigateToHome() {
        navigation.navigate('Home', {
            screen: 'Settings'
        })
    }

    return (
      <View>
        <Text> index </Text>
        <Button title="navigate" onPress={navigateToHome}/>
      </View>
    );
}
