import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

import HomeScreen from './pages/Home'
import SobreScreen from './pages/Sobre'

import DashboardRoutes from './routes/dashboard.routes';

function Routes() {
    return (    
        <Stack.Navigator initialRouteName="Sobre" screenOptions={{ headerStyle: { backgroundColor: '#7159c1' }, headerTintColor: '#FFF' }}>
            <Stack.Screen name="Home" options={{ title: 'Dashboard' }} component={DashboardRoutes} />
            <Stack.Screen name="Sobre" component={SobreScreen} />
        </Stack.Navigator>
    );
}

export default Routes;